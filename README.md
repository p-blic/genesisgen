***
# Generador de blocs inicials
Aquest programa és un fork de [*Genesis block generator*](https://github.com/liveblockchain/genesisgen.git), amb alguna correcció i afegiment. Es pot llegir el [README original](./README-original.md)  per si es necessita consultar.

## Compilar

	$ make
## Executar

	$ ./genesis <pubkey> "<timestamp>" <nBits> <startNonce> <unixtime> <reward>

####Arguments obligatoris:
* **pubkey**: Clau pública.
* **timestamp**: Missatge a fer servir com a coinbase.
* **nBits**: Dificultat.
####Arguments opcionals
* **startNonce**: Nonce inicial per començar a calcular el hash. Normalment és 0.
* **unixtime**: La data en format UNIX. 0 per la data actual.
* **reward**: La recompensa en unitats de la moneda pel minat del bloc inicial. Per defecte 50.

***
***
#### [ToDo]
* Llegir dades des dels fitxers per continuar un minat llarg.
